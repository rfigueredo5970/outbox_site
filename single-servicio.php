<?php
/**
 * Template Name: Servicio Template
 * Template Post Type: servicio
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Outbox Site
 * @since Outbox Site 1.0.3
 * 
 *
 */

	if('servicio' == get_post_type()){
	
		get_header('single-servicio');
	}else{
	
		get_header();
	}
?>
<section class="single-servicio">
	<div class="container-fluid">
    	<div class="row">
		<?php
			if (have_posts()) : while(have_posts()) : the_post();

			$post = get_post();	

			$icono_servicio = get_post_meta($post->ID, 'icono_servicio', true); 
;
		?>		
       			<article class="icono-single-servicio col-xs-12 col-xs-offset-0 col-sm-12 col-sm-offset-0 col-md-3 col-md-offset-1">
       				
       				<?php 
       				if ( '' != $icono_servicio ) {
    					echo wp_get_attachment_image( $icono_servicio, 'thumbnail', "", array( "class" => "img-responsive aligncenter center-block" ) );
								} else { 
       								echo '<img src="' . get_bloginfo( 'template_url' ) . '/img/ico_servicio_acreditacion.png" alt="icono-servicio" class="img-responsive aligncenter center-block">';
								}
       				?>       				
       			</article>

       			<article class="col-xs-12 col-sm-12 col-md-offset-3 col-md-8">	
				<h2 class="article-title"><?php the_title();?></h2>

				<!-- Image of the article -->
				<?php
					if ( has_post_thumbnail() ) {
						
							the_post_thumbnail('list_servicios_thumbs', array('class' => 'thumb  '));

					} else{
            			echo '<img src="' . get_bloginfo( 'template_url' ) . '/img/default400x300.png" alt="Default Image" class="thumb img-responsive aligncenter center-block">';
          					}
				?>
                 
                 <!-- Text of the article-->
                 <span>        	
                	<?php the_content(); ?>
                 </span>
              	</article>

			<?php endwhile; else: ?>		
				No se encontraron servicios
			<?php endif; ?>

		</div> <!-- end row -->
	</div> <!-- end container-fluid -->
</section>
 <?php 
 
		get_footer();
	

