<?php

add_action( 'customize_register', 'outbox_site_customize_register' );

function outbox_site_customize_register( $wp_customize ) {

	/*
	 * Failsafe is safe
	 */
	if ( ! isset( $wp_customize ) ) {
		return;
	}

	// Banner intro
	$wp_customize->add_section('outbox_site_bienvenidos',array(
		'title'=>"Banner de Entrada"
	));

	$wp_customize->add_setting('outbox_site_bienvenidos_button_text',array(
		'default'=>'Más Información'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_bienvenidos_button_text_control',array( 
		'label'=> 'Texto del botón',
		'section' => 'outbox_site_bienvenidos',
		'settings' => 'outbox_site_bienvenidos_button_text'
	)));

	$wp_customize->add_setting('outbox_site_bienvenidos_button_url',array(
		'default'=>'#servicios'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_bienvenidos_button_url_control',array( 
		'label'=> 'URL del botón',
		'section' => 'outbox_site_bienvenidos',
		'settings' => 'outbox_site_bienvenidos_button_url'
	)));

	// Titulo Principal de la Pagina
		$wp_customize->add_section('outbox_site_h1',array(
		'title'=>"Titulo Principal de la página"
	));

	$wp_customize->add_setting('outbox_site_h1_title',array(
		'default'=>'Outbox S.P.A'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_h1_title_control',array( 
		'label'=> 'Título',
		'section' => 'outbox_site_h1',
		'settings' => 'outbox_site_h1_title'
	)));

	// Acerca de Nosotros
	
	$wp_customize->add_section('outbox_site_about',array(
		'title'=>"Acerca de Nosotros"
	));

	$wp_customize->add_setting('outbox_site_about_title1',array(
		'default'=>'Acerca de Nosotros'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_about_title1_control',array( 
		'label'=> 'Primer Título',
		'section' => 'outbox_site_about',
		'settings' => 'outbox_site_about_title1'
	)));



	$wp_customize->add_setting('outbox_site_about_text1',array(
		'default'=>"Somos considerados un outsourcing de servicios especializados en áreas
                vitales para el contínuo y óptimo funcionamiento de las operaciones diarias de nuestros clientes."
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_about_text1',array( 
		'label'=> 'Primer Párrafo',
		'section' => 'outbox_site_about',
		'settings' => 'outbox_site_about_text1',
		'type'=>'textarea'
	)));

	$wp_customize->add_setting('outbox_site_about_title2',array(
		'default'=>'Nuestro Objetivo'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_about_title2_control',array( 
		'label'=> 'Segundo Título',
		'section' => 'outbox_site_about',
		'settings' => 'outbox_site_about_title2'
	)));

	$wp_customize->add_setting('outbox_site_about_text2',array(
		'default'=>"Brindar a nuestros clientes la confianza y comodidad de tener un departamento de servicios generales externo; siempre dispuesto a brindar el mejor soporte con una atención rápida y de calidad."
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_about_text2',array( 
		'label'=> 'Segundo Párrafo',
		'section' => 'outbox_site_about',
		'settings' => 'outbox_site_about_text2',
		'type'=>'textarea'
	)));

	// Misión y Visión

	$wp_customize->add_section('outbox_site_mision',array(
		'title'=>"Misión y Visión"
	));

		$wp_customize->add_setting('outbox_site_mision_title1',array(
		'default'=>'Misión'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_mision_title1_control',array( 
		'label'=> 'Misión',
		'section' => 'outbox_site_mision',
		'settings' => 'outbox_site_mision_title1'
	)));



	$wp_customize->add_setting('outbox_site_mision_text1',array(
		'default'=>"Cumplir los requerimientos de los clientes, a través de la prestación de nuestros servicios,
                  bajo un modelo de atención profesional y perzonalizado."
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_mision_text1',array( 
		'label'=> 'Descripción de la Misión',
		'section' => 'outbox_site_mision',
		'settings' => 'outbox_site_mision_text1',
		'type'=>'textarea'
	)));

	$wp_customize->add_setting('outbox_site_mision_title2',array(
		'default'=>'Visión'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_mision_title2_control',array( 
		'label'=> 'Visión',
		'section' => 'outbox_site_mision',
		'settings' => 'outbox_site_mision_title2'
	)));

	$wp_customize->add_setting('outbox_site_mision_text2',array(
		'default'=>"Consolidarnos como empresa cuyos servicios sean la imagen del talento individual de su capital humano."
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_mision_text2',array( 
		'label'=> 'Descripción de la Visión',
		'section' => 'outbox_site_mision',
		'settings' => 'outbox_site_mision_text2',
		'type'=>'textarea'
	)));

	$wp_customize->add_setting('outbox_site_mision_imagen',array(
			'default'		=> get_stylesheet_directory_uri() . '/img/fondo_mision_vision.jpg',
			'sanitize_callback'	=> 'esc_url_raw',
			'transport'		=> 'postMessage'
	));

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			// $wp_customize object
			$wp_customize,
			// $id
			'outbox_site_mision_imagen',
			// $args
			array(
				'settings'		=> 'outbox_site_mision_imagen',
				'section'		=> 'outbox_site_mision',
				'label'			=>  'Banner de Misión y Visión',
				'description'	=>  'Selecciona la Imagen de Fondo para la sección de Misión y Visión'
			)
		)
	);



	// Servicios
	$wp_customize->add_section('outbox_site_servicios',array(
		'title'=>"Servicios"
	));

	$wp_customize->add_setting('outbox_site_servicios_maintitle',array(
		'default'=>'Servicios'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_servicios_maintitle_control',array( 
		'label'=> 'Título Principal',
		'section' => 'outbox_site_servicios',
		'settings' => 'outbox_site_servicios_maintitle'
	)));

	$wp_customize->add_setting('outbox_site_servicios_secondarytitle',array(
		'default'=>'Servicios Empresariales B&D Asociados OUTBOX SPA'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_servicios_secondarytitle_control',array( 
		'label'=> 'Título Secundario',
		'section' => 'outbox_site_servicios',
		'settings' => 'outbox_site_servicios_secondarytitle'
	)));

	// Aliados
	$wp_customize->add_section('outbox_site_alianzas',array(
		'title'=>"Alianzas Comerciales"
	));

	$wp_customize->add_setting('outbox_site_alianzas_maintitle',array(
		'default'=>'Alianzas Comerciales'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_alianzas_maintitle_control',array( 
		'label'=> 'Título Principal',
		'section' => 'outbox_site_alianzas',
		'settings' => 'outbox_site_alianzas_maintitle'
	)));

	//Contacto
	$wp_customize->add_section('outbox_contacto',array(
		'title'=>"Contacto"
	));

	$wp_customize->add_setting('outbox_site_contacto_maintitle',array(
		'default'=>'Alianzas Comerciales'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_contacto_maintitle_control',array( 
		'label'=> 'Título Principal',
		'section' => 'outbox_contacto',
		'settings' => 'outbox_site_contacto_maintitle'
	)));

	$wp_customize->add_setting('outbox_site_contacto_secondarytitle',array(
		'default'=>'Si desea cotizar o solicitar más información: '
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_contacto_secondarytitle_control',array( 
		'label'=> 'Título Secundario',
		'section' => 'outbox_site_contacto',
		'settings' => 'outbox_site_contacto_secondarytitle'
	)));

	$wp_customize->add_setting('outbox_site_contacto_addresstitle',array(
		'default'=>'Dirección'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_contacto_addresstitle_control',array( 
		'label'=> 'Título Dirección',
		'section' => 'outbox_site_contacto',
		'settings' => 'outbox_site_contacto_addresstitle'
	)));

	$wp_customize->add_setting('outbox_site_contacto_addresstext',array(
		'default'=>'Marchant Pereira #150, Providencia. Oficina 901. Santiago de Chile'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_contacto_addresstext_control',array( 
		'label'=> 'Parráfo Dirección',
		'section' => 'outbox_site_contacto',
		'settings' => 'outbox_site_contacto_addresstext'
	)));

	$wp_customize->add_setting('outbox_site_contacto_phonetitle',array(
		'default'=>'Teléfono'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_contacto_phonetitle_control',array( 
		'label'=> 'Título Teléfono',
		'section' => 'outbox_site_contacto',
		'settings' => 'outbox_site_contacto_phonetitle'
	)));

	$wp_customize->add_setting('outbox_site_contacto_phonetext',array(
		'default'=>'+56 9 3190 9117'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_contacto_phonetext_control',array( 
		'label'=> 'Número de Teléfono',
		'section' => 'outbox_site_contacto',
		'settings' => 'outbox_site_contacto_phonetext'
	)));

	$wp_customize->add_setting('outbox_site_contacto_emailtitle',array(
		'default'=>'Correo'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_contacto_emailtitle_control',array( 
		'label'=> 'Título Correo',
		'section' => 'outbox_site_contacto',
		'settings' => 'outbox_site_contacto_emailtitle'
	)));

	$wp_customize->add_setting('outbox_site_contacto_emailtext',array(
		'default'=>'administrador@outbox.com'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_contacto_emailtext_control',array( 
		'label'=> 'Dirección de Correo Electrónico',
		'section' => 'outbox_site_contacto',
		'settings' => 'outbox_site_contacto_emailtext'
	)));


	// Pagina Simple
	

	$wp_customize->add_section('outbox_site_single',array(
		'title'=>"Pagina Simple"
	));

		$wp_customize->add_setting('outbox_site_single_title1',array(
		'default'=>'Outbox SPA'
	));

	$wp_customize->add_control(new WP_Customize_Control($wp_customize,'outbox_site_single_title1_control',array( 
		'label'=> 'Titulo por defecto para página simple',
		'section' => 'outbox_site_single',
		'settings' => 'outbox_site_single_title1'
	)));

	$wp_customize->add_setting('outbox_site_single_imagen',array(
			'default'		=> get_stylesheet_directory_uri() . '/img/banner_02.jpg',
			'sanitize_callback'	=> 'esc_url_raw',
			'transport'		=> 'postMessage'
	));

	$wp_customize->add_control(
		new WP_Customize_Image_Control(
			// $wp_customize object
			$wp_customize,
			// $id
			'outbox_site_single_imagen',
			// $args
			array(
				'settings'		=> 'outbox_site_single_imagen',
				'section'		=> 'outbox_site_single',
				'label'			=>  'Banner de Página Simple',
				'description'	=>  'Selecciona la Imagen de Fondo para el banner de las páginas simples (ex: Post Individual, Servicio Individual, Error $404'
			)
		)
	);




}
