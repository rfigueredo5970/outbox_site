<?php
/**
 * The template for choose single template 
 * @package WordPress
 * @subpackage Outbox Site
 * @since Outbox Site 1.0.3
 */



if( have_posts() ){ the_post(); rewind_posts(); }



if('servicio' == get_post_type()){

	include( TEMPLATEPATH . '/single-servicio.php');
}else{

	include(TEMPLATEPATH . '/single-post.php');
}

