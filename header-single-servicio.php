<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="description" content="Outbox S.P.A Website">
  <meta name="keywords" content="Outbox, Servicio Empresarial, Gestión de Importación y Aduana">
  <meta name="author" content="Rafael Figueredo,  Netsi Iriza">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- pagetitle -->
	<title><?php wp_title(' | ', 'echo', 'right'); ?><?php bloginfo('name'); ?> </title>
	
	<!-- stylesheets -->
	<link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url')?>/css/normalize.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.6.0/tiny-slider.css">
  <link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url')?>/css/bootstrap.min.css">
  <link type="text/css" rel="stylesheet" href="<?php bloginfo('stylesheet_url')?>">
  <link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url')?>/css/single-servicio.css">
  <link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url')?>/css/responsive.css">
  <link type="text/css" rel="stylesheet" href="<?php bloginfo('template_url')?>/css/responsive-single-servicio.css">
  <style>
      <?php
      if ( get_theme_mod( 'outbox_site_single_imagen',  get_stylesheet_directory_uri() . '/img/banner_02.jpg' ) ) {
        $outbox_site_single_imagen_url = get_theme_mod( 'outbox_site_single_imagen', get_stylesheet_directory_uri() . '/img/banner_02.jpg' );
      } else {
        $outbox_site_single_imagen_url = get_stylesheet_directory_uri() . '/img/banner_02.jpg';
      }
    ?>
        body {
          background-image: url(<?php bloginfo('template_url')?>/img/banner_02.jpg);
        }   
  </style>
</head>
<body>

	<div id="inicio"></div>
<header>
  <nav class="menu">
    <a class="navbar-brand " href="<?php bloginfo('url'); ?>">
        <img src="<?php bloginfo('template_url')?>/img/logo.png"  max-heigth="150px" alt="Logo Outbox S.P.A" class="img-responsive">
    </a>
  
          <?php /* Primary navigation */
            wp_nav_menu( array(
              'menu' => 'header_menu',
              'theme_location' => 'header_menu',
              'depth' => 1,
              'container' => false,
              'menu_class' => 'active',
              'item-spacing'=>'discard')
            );
      ?>      
   <a class="toggle-nav-menu" id="toggle-nav-menu" href="#"><i class="fas fa-bars"></i></a>
    
  </nav> 
	<?php wp_head(); ?> 
	</header>	

	