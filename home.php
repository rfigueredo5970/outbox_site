<?php 
  get_header();

  	//response generation function
	$response = ""; 


if(isset($_POST['submit'])){

	//response generation function
	  $response = "OK";
	 
	  //function to generate response
	  function my_contact_form_generate_response($type, $message){
	 
	    global $response;
	 
	    if($type == "success") $response = "<div class='success'>{$message}</div>";
	    else $response = "<div class='error'>{$message}</div>";
	 
	  }
	
	  //response messages
	$missing_content = "Por favor rellenar todos los campos";
	$email_invalid   = "La dirección de correo es inválida";
	$message_unsent  = "El mensaje no pudo ser enviado. Intente de nuevo.";
	$message_sent    = "¡Gracias! Su mensaje ha sido enviado.";
	 
	//user posted variables
	$name = $_POST['your-name'];
	$email = $_POST['your-email'];
	$website = $_POST['your-website'];
	$phone = $_POST['your-phone'];
	$subject = $_POST['subject'];
	$message = $_POST['your-message'];

	//php mailer variables
	$to = get_option('admin_email');
	$subject = "Someone sent a message from " . get_bloginfo('name');
	$headers = 'From: ' . $email . "\r\n" .
	  'Reply-To: ' . $email . "\r\n";
	
	 if ($_POST['submitted']!=1){
	
		 my_contact_form_generate_response("error", $missing_content);
	 
	 }elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {	
	//validate email
		my_contact_form_generate_response("error", $email_invalid);
	
	 }elseif (empty($name) || empty($message)) {
	 //email is valid
	 //validate presence of name and message	
	
		my_contact_form_generate_response("error", $missing_content);
	
	 }else{
	 	//send email
	 	my_contact_form_generate_response("success", $message_sent); //message sent!
	 }
}
?>	
  <section class="banner-intro">
  <?php  wp_reset_query() ?>
 <?php $slides_query = new WP_Query(  array( 'post_type' => 'slide', 'post_status' => 'publish', 'order' => 'ASC') ); ?>
    <div class="container-fluid">
      <div id="background-carousel">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
      <?php if ( $slides_query->have_posts() ) : while ( $slides_query->have_posts() ) : $slides_query->the_post(); 
          $post = get_post();
          if(($slides_query->current_post ) == 0) :
        ?>  

        <?php
          if ( has_post_thumbnail() ) {
        ?>       
        <div class="item active" style="background-image:url(<?php the_post_thumbnail_url(); ?>)">
          
          <?php
          } else{
            ?>
          <div class="item active" style="background-image:url(<?php echo get_bloginfo( 'template_url' ) ?>/img/banner_01.jpg)">
          <?php
          }
        ?>
          <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12">
              <div class="intro-message">
               <h2 class="h-active"><?php the_title(); ?></h2>
               <h2><?php the_excerpt(); ?></h2>
            </div> <!-- end intro-message -->
           </article>
           </div> <!-- end row -->
        </div>
      <?php else: ?>

        
          
        <?php
          if (has_post_thumbnail() ) {
        ?>       
        <div class="item" style="background-image:url(<?php the_post_thumbnail_url(); ?>)">
          
          <?php
          } else{
            ?>
          <div class="item" style="background-image:url(<?php echo get_bloginfo( 'template_url' ) ?>/img/banner_02.jpg)">">
          <?php
          }
        ?>


          <div class="row">
            <article class="col-xs-12 col-sm-12 col-md-12">
              <div class="intro-message">
                <h2 class="h-active"><?php the_title(); ?></h2>
                <h2 ><?php the_excerpt(); ?></h2>
              </div> <!-- end intro-message -->
            </article> 
            </div> <!-- end row -->
          </div> <!-- end item -->

          <?php endif; endwhile; else: ?>   
        <h4>No se encontraron imagenes del banner</h4>
        <?php endif; ?>
        <?php  wp_reset_query() ?>
      </div> <!--  end carousel-inner -->
    </div> <!-- end #myCarousel -->
</div> <!-- end background-carousel -->
 
 
<div id="content-wrapper">
<!-- PAGE CONTENT -->
    <div class="container-fluid">
      <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12">
          <div class="intro-message">
            <ul class="list-inline">
              <li>
                <a href="<?php echo get_theme_mod('outbox_site_bienvenidos_button_url','#servicios');?>" class="btn btn-info btn-lg"><?php echo get_theme_mod('outbox_site_bienvenidos_button_text','Más Información');?></a>
              </li>
            </ul>
          </div>
        </article>
      </div>
    </div><!-- End Container -->
<!-- PAGE CONTENT -->
</div>
    </div>
  </section>
  <h1><?php echo get_theme_mod('outbox_site_h1_title','Outbox S.P.A.');?></h1>
  <section class="about" id="about">
    <div class="container-fluid">
      <div class="row">            
              <article class="col-xs-12 col-sm-10 col-md-7">
                <h3><?php echo get_theme_mod('outbox_site_about_title1','Acerca de Nosotros');?></h3>
                <p>
                 <?php echo get_theme_mod( 'outbox_site_about_text1', ' Somos considerados un outsourcing de servicios especializados en áreas vitales para el contínuo y óptimo funcionamiento de las operaciones diarias de nuestros clientes.' );?>
                </p>
              </article>
              <article class="col-xs-12 col-sm-10 col-md-7">
                <h3><?php echo get_theme_mod('outbox_site_about_title2','Nuestro Objetivo');?></h3>
                <p>
                 <?php echo get_theme_mod( 'outbox_site_about_text2', 'Brindar a nuestros clientes la confianza y comodidad de tener un departamento de servicios generales externo; siempre dispuesto a brindar el mejor soporte con una atención rápida y de calidad.' );?>
                </p>
              </article>            
        </div>
    </div>
  </section>
 <section class="mision" id="mision">
    <div class="container-fluid">
      <div class="row">
              <article class="col-xs-12 col-sm-6 col-md-6">
                <h3><?php echo get_theme_mod('outbox_site_mision_title1','Misión');?></h3>
                <p>
                	<?php echo get_theme_mod('outbox_site_mision_text1','Cumplir los requerimientos de los clientes, a través de la prestación de nuestros servicios,
                  bajo un modelo de atención profesional y perzonalizado.');?>
                </p>
              </article>
              <article class="col-xs-12 col-sm-6 col-md-6">
                <h3><?php echo get_theme_mod('outbox_site_mision_title2','Visión');?></h3>
                <p>
                	<?php echo get_theme_mod('outbox_site_mision_text2','Consolidarnos como empresa cuyos servicios sean la imagen del talento individual de su capital humano.');?>
                  
                </p>
              </article>   
      </div>
    </div>
  </section>
  <section class="servicios" id="servicios">
    <h2><?php echo get_theme_mod('outbox_site_servicios_maintitle','Servicios');?></h2>
    <p><?php echo get_theme_mod('outbox_site_servicios_secondarytitle','Servicios Empresariales B&D Asociados OUTBOX SPA');?></p>

    <div class="container-fluid">
      <?php  wp_reset_query() ?>
      <?php $servicios_query = new WP_Query(  array( 'post_type' => 'servicio', 'posts_per_page' => 10, 'post_status' => 'publish', 'order' => 'DESC' ) ); ?>
      <div class="row">
      	<?php 

      			if ( $servicios_query->have_posts() ) : while ( $servicios_query->have_posts() ) : $servicios_query->the_post(); 
					$post = get_post();				

		?>
                <article class="col-xs-12 col-sm-5 col-md-5">
          <?php
					if ( has_post_thumbnail() ) {
						
							the_post_thumbnail('list_servicios_thumbs', array('class' => 'thumb img-responsive aligncenter center-block'));

					} else{
            echo '<img src="' . get_bloginfo( 'template_url' ) . '/img/default400x300.png" alt="Default Image" class="thumb img-responsive aligncenter center-block">';
          }
				  ?>
                 
                <h3><?php the_title();?></h3>
                
                  <?php the_excerpt(); ?>
                
                <a href="<?php echo get_the_permalink() ?>" class="btn btn-primary">+Más info</a>
              </article>
    		 <?php endwhile; else: ?>		
				No se encontraron servicios
			<?php endif; ?>
    		<?php  wp_reset_query() ?>
      </div>
    </div>
  </section>
  <section class="alianzas" id="alianzas">
     <h2><?php echo get_theme_mod('outbox_site_servicios_maintitle','Alianzas Comerciales');?></h2>

    <div class="container-fluid" >
    
        
      <?php $aliados_query = new WP_Query(  array( 'post_type' => 'aliado', 'post_status' => 'publish', 'order' => 'ASC') ); ?>
      <div class="row">
        <div class="controls customized-arrows">
            <button class="prev">
        <i class="fa fa-angle-left  slider-btn-icon" aria-hidden="true"></i>
        </button>
        <button class="next">
        <i class="fa fa-angle-right slider-btn-icon slider-next" aria-hidden="true"></i>
        </button>
      </div>
        <div class="my-slider">

          <?php 
          
            if ( $aliados_query->have_posts() ) : while ( $aliados_query->have_posts() ) : $aliados_query->the_post(); 
            $post = get_post();
         
            $url_link = get_post_meta($post->ID, 'url', true); 
            
            if(!$url_link)
            {
              $url_link = get_the_permalink();
            }       

          ?>
          <article class="col-xs-11 col-sm-5 col-md-3">
            <?php 
            if ( has_post_thumbnail() ) {
              echo '<a href=" ' . esc_url($url_link) . '" target="_blank" >';
                the_post_thumbnail('list_aliados_thumbs', array('class' => 'thumb img-responsive aligncenter   center-block')); 
              echo '</a>';

          }
          else{
            echo '<a href=" ' . esc_url($url_link) . ' " target="_blank" >';
              echo '<img src="' . get_bloginfo( 'template_url' ) . '/img/aliados_01.png" width="129" height="160" alt="Default Image" class="thumb img-responsive aligncenter center-block wp-post-image" >'; 
             echo ' </a>';
          }
          ?>
              <h5><?php the_title()?></h5>
              <p><?php the_excerpt(); ?></p>
            </article>
             <?php endwhile;  else: ?>    
                No se encontraron aliados
            <?php endif; ?>
            <?php  wp_reset_query() ?>
        </div>      
      </div> 
      <!-- End row -->
      
    </div>
  </section>
  <section class="contacto" id="contacto">
    <h2><?php echo get_theme_mod('outbox_site_contacto_maintitle','Contacto');?></h2>
    <div class="container-fluid">
      <div class="row">            
        <div class="col-xs-12 col-sm-5 col-sm-offset-1 col-md-4">
          
          <article>
            <h3><i class="fas fa-map-marker-alt icon-color"></i> <?php echo get_theme_mod('outbox_site_contacto_addresstitle','Dirección');?></h3>
            <p>
              <?php echo get_theme_mod('outbox_site_contacto_addresstext','Marchant Pereira #150, Providencia. Oficina 901. Santiago de Chile');?>
          </p>
          </article>
          <article>
            <h3><i class="fas fa-phone icon-color"></i> <?php echo get_theme_mod('outbox_site_contacto_phonetitle','Teléfono');?></h3>
            <p><?php echo get_theme_mod('outbox_site_contacto_phonetext','+56 9 3190 9117');?></p>
          </article>
          <article>
            <h3><i class="fas fa-envelope icon-color"></i> <?php echo get_theme_mod('outbox_site_contacto_emailtitle','Correo');?></h3>
            <p><?php echo get_theme_mod('outbox_site_contacto_emailtext','administrador@outbox.com');?></p>
          </article>
          </div>

        <div class="col-xs-12 col-sm-5 col-md-6  form-container">
        <span><?php echo get_theme_mod('outbox_site_contacto_secondarytitle','Si desea cotizar o solicitar más información: ');?></span>
        	<div class="row">
        		<div class="panel">
        			<div class="panel-body">
						<?php echo do_shortcode( '[contact-form-7 id="69" title="Contacto Outbox Site"]' ); ?>
        			</div> <!-- End Panel Body -->
        		</div>  <!-- End Panel  -->
        	</div>  <!-- End Row -->

        </div>
       
          
      </div>
    </div>
  </section>
<!-- Footer -->
<?php
  get_footer();
?> 