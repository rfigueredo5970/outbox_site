
		
  var slider = tns({
    container: '.my-slider',
    items: 1,
    slideBy: 'page',
    autoplay: false,
    nav:false,
    arrowKeys:true,
    // mouseDrag:true,
    controlsContainer: ".customized-arrows",
    controlsText: ['prev', 'next'],
        responsive: {
         	
      768: {
        items: 2
      },
      992: {
        items: 4
      }
    }
  });
  
  // NOTE: 
  // prior to v2.0.2, options "container", "controlsContainer", "navContainer" and "autoplayButton" still need to be DOM elements.
  // e.g. container: document.querySelector('.my-slider'),
	
