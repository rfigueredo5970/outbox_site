<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Outbox Site
 * @since Outbox Site 1.0.3
 */

get_header('error404'); ?>

<div class="wrap">
	<div id="primary" class="content-area container">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<div class="row">
					<article class="col-xs-12 col-sm-12 col-md-12">
						
						<header class="page-header">
							<h1 class="page-title">No se encontró la página</h1>
						</header><!-- .page-header -->
						<div class="page-content">
							<p> La página solicitada no fue localizada</p>

						</div><!-- .page-content -->
					</article>
				</div> <!-- row -->
			</section><!-- .error-404 -->
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();
