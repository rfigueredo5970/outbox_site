<?php

include('customizer.php');

	
@ini_set( 'upload_max_size' , '20M' );
@ini_set( 'post_max_size', '20M');
@ini_set( 'max_execution_time', '300' );
	



	register_nav_menus( array(
        'header_menu' => __( 'Header Menu', 'outbox_site' ),
        'social_bar' =>__( 'Social Bar', 'outbox_site' ),
	) );


	// Add Images From Post 
	add_theme_support('post-thumbnails');
	add_image_size( 'list_articles_thumbs', 400, 300, true );
	add_image_size( 'list_servicios_thumbs', 400, 300, true );
	add_image_size( 'list_aliados_thumbs', 129, 160, true );


	/**
 * Writes the  background image out to the 'head' element of the document
 * by reading the value from the theme mod value in the options table.
 */
function outbox_site_mision_imagen_customizer_css() {
?>
	<style type="text/css">
		<?php
			if ( get_theme_mod( 'outbox_site_mision_imagen',  get_stylesheet_directory_uri() . '/img/fondo_mision_vision.jpg' ) ) {
				$outbox_site_mision_imagen_url = get_theme_mod( 'outbox_site_mision_imagen', get_stylesheet_directory_uri() . '/img/fondo_mision_vision.jpg' );
			} else {
				$outbox_site_mision_imagen_url = get_stylesheet_directory_uri() . '/img/fondo_mision_vision.jpg';
			}
		?>
				.mision {
					background-image: url( <?php echo $outbox_site_mision_imagen_url; ?> );
				}
	 </style>
<?php
} // end site customizer_css
add_action( 'wp_head', 'outbox_site_mision_imagen_customizer_css');



/**
 * Registers the Theme Customizer Preview with WordPress.
 */
function outbox_site_customizer_live_preview() {
	wp_enqueue_script(
		'theme-mision-customizer',
		get_stylesheet_directory_uri() . '/js/theme-mision-customizer.js',
		array( 'jquery','customize-preview' ),
		'0.1.0',
		true
	);
} // end customizer_live_preview
add_action( 'customize_preview_init', 'outbox_site_customizer_live_preview',0 );


/**
 * Register Aliados Custom Post Type
 */

// Our custom post type function

if( !post_type_exists( 'aliado' ))
{

function create_aliados_posttype() {
 
    register_post_type( 'aliado',
    // CPT Options
        array(
            'labels' => array(
                'name' => _x('Aliados', 'post type general name'),
                'singular_name' => _x('Aliado', 'post type singular name'),
                'add_new' => _x('Agregar nuevo', 'aliado'),
				'add_new_item' => __('Agregar nuevo aliado'),
				'edit_item' => __('Editar aliado'),
				'new_item' => __('Nuevo aliado'),
				'all_items' => __('Todos los aliados'),
				'view_item' => __('Ver aliado'),
				'search_items' => __('Buscar aliado'),
				'not_found' =>  __('Ningún aliado encontrado'),
				'not_found_in_trash' => __('No se encontraron aliados en la papelera'), 
				'parent_item_colon' => '',
				'menu_name' => __('Aliados')

            ),
    		'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true, 
		    'show_in_menu' => true, 
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'hierarchical' => false,
		    'menu_position' => null,
    		'supports' => array( 'title', 'editor','revisions', 'url','author', 'thumbnail', 'custom-fields','excerpt', 'comments' ),
            'has_archive' => true,
            'rewrite' => array('slug' => 'alianzas-comerciales')
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_aliados_posttype',0 );
}	

/**
 * Register Servicios Custom Post Type
 */
if( !post_type_exists( 'servicio' ) )
{

// Our custom post type function
function create_servicios_posttype() {
 
    register_post_type( 'servicio',
    // CPT Options
        array(
            'labels' => array(
                'name' => _x('Servicios', 'post type general name'),
                'singular_name' => _x('Servicio', 'post type singular name'),
                'add_new' => _x('Agregar nuevo', 'servicio'),
				'add_new_item' => __('Agregar nuevo servicio'),
				'edit_item' => __('Editar servicio'),
				'new_item' => __('Nuevo servicio'),
				'all_items' => __('Todos los servicios'),
				'view_item' => __('Ver servicio'),
				'search_items' => __('Buscar servicio'),
				'not_found' =>  __('Ningún servicio encontrado'),
				'not_found_in_trash' => __('No se encontraron servicios en la papelera'), 
				'parent_item_colon' => '',
				'menu_name' => __('Servicios')
            ),
    		'public' => true,
		    'publicly_queryable' => true,
		    'show_ui' => true, 
		    'show_in_menu' => true, 
		    'query_var' => true,
		    'rewrite' => true,
		    'capability_type' => 'post',
		    'hierarchical' => false,
		    'menu_position' => null,
    		'supports' => array( 'title', 'editor','revisions', 'author', 'thumbnail', 'custom-fields','excerpt', 'comments' ),
            'has_archive' => true,
            'rewrite' => array('slug' => 'servicio'),
        )
    );
}
// Hooking up our function to theme setup
add_action( 'init', 'create_servicios_posttype',0 );
}

/**
 * Register Slides Custom Post Type
 */

// Our custom post type function

if( !post_type_exists( 'slide' ))
{

function cptui_register_my_cpts_slide() {

	/**
	 * Post Type: Slides.
	 */

	$labels = array(
		"name" => __( "Slides", "" ),
		"singular_name" => __( "Slide", "" ),
	);

	$args = array(
		"label" => __( "Slides", "" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => false,
		"rest_base" => "",
		"has_archive" => false,
		"show_in_menu" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "slide", "with_front" => true ),
		"query_var" => true,
		"supports" => array( "title", "thumbnail", "excerpt" ),
		"taxonomies" => array( "category" ),
	);

	register_post_type( "slide", $args );
}

add_action( 'init', 'cptui_register_my_cpts_slide' );

}	





