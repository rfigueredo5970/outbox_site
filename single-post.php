<?php
/**
 * Template Name: Servicio Template
 * Template Post Type: servicio
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Outbox Site
 * @since Outbox Site 1.0.3
 * 
 *
 */	
		get_header('single-servicio');
?>

<section class="single-servicio">
	<div class="container">
    	<div class="row">
		<?php
			if (have_posts()) : while(have_posts()) : the_post();

			$post = get_post();	

		?>		
       		
				<div class="col-xs-12 col-sm-12 col-md-3"></div>
       			<article class="col-xs-12 col-sm-12  col-md-8">	
				<h1 class="article-title"><?php the_title();?></h1>

				<!-- Image of the article -->
				<?php
					if ( has_post_thumbnail() ) {
						
							the_post_thumbnail('list_servicios_thumbs', array('class' => 'thumb  '));

					} 
				?>
                 
                 <!-- Text of the article-->
                 <span>        	
                	<?php the_content(); ?>
                 </span>
              	</article>

			<?php endwhile; else: ?>		
				No se encontraron publicaciones
			<?php endif; ?>

		</div> <!-- end row -->
	</div> <!-- end container-fluid -->
</section>
 <?php 
	get_footer();
	
