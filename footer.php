<footer>
<!--   <div class="clearfix"></div> -->
  
    <div class="container-fluid social-bar-container">
      <nav class="social-bar">
        <?php /* Primary navigation */
            wp_nav_menu( array(
              'menu' => 'social_bar',
              'theme_location' => 'social_bar',
              'depth' => 1,
              'container' => false,
              'menu_class' => 'active list-inline foot')
            );
      ?>      
      </nav>
    </div>
    <span>Copyright 2017 | Todos los derechos reservados | Outbox S.P.A.</span>
     <div class="made-by">
        Elaborado por: <a href="http://www.elaraguaneydigital.com.ve/" target="_blank">EAD Group</a> | <a href="mailto:elaraguaneydigital@gmail.com">elaraguaneydigital@gmail.com</a>
    </div>
</footer>
<?php wp_footer(); ?>
  <!-- js -->
  <script type="text/javascript" src="<?php bloginfo('template_url')?>/js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url')?>/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url')?>/js/smoothscrolling.jquery.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url')?>/js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url')?>/js/menu-toggle-nav.js"></script>
  <!--[if (lt IE 9)]><script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.6.0/min/tiny-slider.helper.ie8.js"></script><![endif]-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.6.0/min/tiny-slider.js"></script>
  <!-- NOTE: from v2.2.1 tiny-slider.js is no longer required to be in <body> -->
  <script type="text/javascript" src="<?php bloginfo('template_url')?>/js/slider.js"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url')?>/js/background-carousel.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
  <script type="text/javascript" src="<?php bloginfo('template_url')?>/js/script.js"></script>
</body>
</html>